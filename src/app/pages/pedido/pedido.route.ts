import { RouterModule, Routes } from '@angular/router';
import { ListaPedidoComponent } from './lista-pedido.component';



const pedidoRoutes: Routes = [
   { path: 'lista', component: ListaPedidoComponent },
   { path: '', redirectTo: '/pages/pedido/lista', pathMatch: 'full' }

];


export const PEDIDO_ROUTES = RouterModule.forChild(pedidoRoutes);
