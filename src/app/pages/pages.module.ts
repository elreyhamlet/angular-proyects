import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PAGES_ROUTES } from './pages.route';
import { PedidoComponent } from './pedido/pedido.component';


@NgModule({
  declarations: [
    PedidoComponent,
  ],
  imports: [
    CommonModule,
    PAGES_ROUTES

  ]
})
export class PagesModule { }
